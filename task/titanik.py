import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    y = int(round(df[(df['Name']=='Mr.') & (~df['Age'].isnull())]['Age'].median(), 0))
    m = int(round(df[(df['Name']=='Mrs.') & (~df['Age'].isnull())]['Age'].median(), 0))
    n = int(round(df[(df['Name']=='Miss.') & (~df['Age'].isnull())]['Age'].median(), 0))


    x = int(list(df[df['Name']=='Mr.']['Age'].isnull().sum())[0])
    k = int(list(df[df['Name']=='Mrs.']['Age'].isnull().sum())[0])
    l = int(list(df[df['Name']=='Miss.']['Age'].isnull().sum())[0])

    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]


